CONTROLS:

PLAYER 1: [wasd] to move, [f] to fire, [r] to lay bombs
PLAYER 2: [okl;] to move. ['] to fire, [[] to lay bombs

HOW TO PLAY:

Move around the board and shoot each other!

First player to lose all health loses.

Firing at a barrel makes it explode two squares in all four directions.

Note well: barrels can set off other barrels.

Neither players, bullets, nor fire can pass through walls.

Have fun!

TO RUN:

Click on "Tanks".

MAP FILES:

Default maps, such as "Fortress" and "Barricade" are included.

Two create your own maps, copy one of these files and modify it.
(S = spawn point, the rest of the characters are equivalent.)

To run a custom map, make sure that it's in the same folder as the game
files, and enter the full name of the map (e.g. "myMap.txt")